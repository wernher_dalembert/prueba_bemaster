import express from "express";
import cors from 'cors'
import morgan from 'morgan'
import router from "./routes/rutas.js";

const app = express()

app.set('port', 3030)
app.use(cors())
app.use(morgan('dev'))
app.use(express.json())
app.use(router)

export default app