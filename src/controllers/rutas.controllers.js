export const Github = async (req,res) => {
    try {
        await fetch(`https://api.github.com/users/google/repos`).then(res => res.json())
        .then(data => {
            const repositoriosOrdenados = data.sort((a, b) => b.stargazers_count - a.stargazers_count);
            const mas_populares = repositoriosOrdenados.slice(0,10)

            let lista_final = []
            mas_populares.forEach(element => {
                lista_final.push({id: element.id, name: element.name, full_name: element.full_name, stargazers_count: element.stargazers_count})
            });
            // console.log(lista_final)

            res.status(200).json(lista_final)
        })
    } catch (error) {
        console.log(error)
    }
    
}

export const numImpares = (req,res) => {
    const numero = req.params.numero

    if (!isNaN(numero)) {
        let impares = [];

        for (let i = 1; i <= numero; i += 2) {
            impares.push(i);
        }    

        res.status(200).json(impares)
    } else {
        res.status(400).json({mensaje: `no es número`})
    }
}