import { Router } from "express";
import { Github, numImpares } from "../controllers/rutas.controllers.js";

const router = Router()
router.get('/github',Github)
router.get('/numImpares/:numero', numImpares)

export default router
